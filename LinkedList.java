public class LinkedList {
    Node head;
    public class Node {
        int value;
        Node next;
        Node(int a) {
            value = a;
            next = null;
        }
    }
    public void insertAtFirst(int value) {
        Node current = new Node(value);
        current.next = head;
        head = current;
    }


   public void removeAtPosition(int position) {
        if (head == null)
            return;
        Node temp = head;
        if (position == 0) {
            head = temp.next;
            return;
        }
        for (int i=0; temp!=null && i<position-1; i++)
            temp = temp.next;

        if (temp == null || temp.next == null)
            return;
        Node next = temp.next.next;
        temp.next = next;
    }


    public void showList(){
        Node current = head;
        while (current.next != null) {
            System.out.println(current.value);
            current= current.next;
        }
        System.out.println(current.value);
    }
}
