public class Heap {
    public Node Parent;
    public Node C1,C2;

    Heap(){
        this.Parent=null;
        this.C1.left=null;
        this.C2.right=null;
    }

    public void insert(int nodeData){

       Node node = new Node(nodeData);
        if (this.Parent == null) {
            this.Parent = node;
        } else if (this.C1.left==null) {
            this.C1.left = node;
        } else {
            this.C2.right = node;

        }

    }

}
