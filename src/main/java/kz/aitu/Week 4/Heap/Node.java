public class Node {

        public int  data;
        public Node left;
        public Node right;


        public Node(int nodeData) {
            this.data = nodeData;
            this.left = null;
            this.right = null;
        }

}
