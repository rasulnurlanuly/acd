public class Node {
    private int data;
    private Node NextNode;

    public int getData() {
        return data;
    }

    public void setData(int data) {
        data = data;
    }

    public Node getNextNode() {
        return NextNode;
    }

    public void setNextNode(Node nextNode) {
        NextNode = nextNode;
    }

    public Node( int data){

        this.data = data;
    }
}

