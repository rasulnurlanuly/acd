public class Queue {
    private Node head;
    private  Node tail;
    private int size;

    public Queue(){
        this.head = null;
        this.tail = null;
    }

    public Node getHead() {
        return head;
    }

    public void setHead(Node head) {
        this.head = head;
    }

    public Node getTail() {
        return tail;
    }

    public void setTail(Node tail) {
        this.tail = tail;
    }

    public void add(Node node){
        if (head == null ){
            head = node;
            tail = node;
            size ++;
        }
        else{
            tail.setNextNode(node);
            node.setNextNode(null);
            tail = node;
            size++;
        }}
        public void poll () {
            if (head == null){
                return;
            }
            head = head.getNextNode();
            size--;
        }
        public int size(){
        return size;
        }
        public int peek(){
         return head.getData();
        }
    }
