public class Task6 {
    public static int dare (int a, int n){
        if (n!=1){
            return a*(dare(a,n-1));
        }
        return a;
    }

    public void run() {
        Scanner num = new Scanner(System.in);
        int a = num.nextInt();
        int n = num.nextInt();
        int t = dare(a,n);
        System.out.println(t);
    }
}
