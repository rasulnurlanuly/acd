public class Task9 {
    public void run() {
        Scanner num = new Scanner(System.in);
        int n = num.nextInt();
        int k = num.nextInt();
        long t = bc(n)/(bc(k)*bc(n-k));
        System.out.println(t);
    }

    public static long bc (int m){
        if (m<=1){
            return 1;
        } else {
            return m*bc(m-1);
        }
    }

}
