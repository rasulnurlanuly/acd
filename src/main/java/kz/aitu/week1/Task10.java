public class Task10 {
    public void run() {
        Scanner num = new Scanner(System.in);
        int n = num.nextInt();
        int m = num.nextInt();
        int i = euclidien(n,m);
        System.out.println(i);
    }

    public static int euclidien (int n, int m){
        int e;
        if (n%m == 0){
            return m;
        } else {
            e = n%m;
            return euclidien(m,e);
        }
    }
}
