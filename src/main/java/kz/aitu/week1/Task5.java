public class Task5 {
    public static int fibohachi (int r){
        if (r == 1 || r==0){
            return r;
        } else {
            return fibohachi(r-2)+fibohachi(r-1);
        }
    }

    public void run() {
        Scanner num = new Scanner(System.in);
        int r = num.nextInt();
        int t = fibohachi(r);
        System.out.println(t);
    }
}
