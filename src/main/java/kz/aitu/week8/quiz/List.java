package week8;

public class List {
    private Student[] studentList;
    private int size = 0;

    public void add(Student student) {
        size++;
    }

    public void print() {
        for (int i = 0; i < size; i++) {
            System.out.println(studentList[i]);
        }

    }


    //type: name_a name_l age gps
    //order: asc desc
    public void sort(String type, String order) {
        if (type.equals("name_a")) {

        } else if (type.equals("name_l")) {

            if (order.equals("asc")) {
                SortNameLAsc();
            } else {
                sort_name_l_desc();
            }

        } else if (type.equals("age")) {
            if (order.equals("asc")) {
                sort_age_asc();
            } else {
                sort_age_desc();
            }

        } else if (type.equals("gpa")) {
            if (order.equals("asc")) {
                sort_gpa_asc();
            } else {
                sort_gpa_desc();
            }

        }
    }

    public void sort_age_asc(int age[]) {
        int n = age.length;
        for (int i = 0; i < n - 1; ++i) {
            for (int j = 0; j < n - i - 1; ++j) {
                if (age[j] > age[j + 1]) {

                    int temp = age[j];
                    age[j] = age[j + 1];
                    age[j + 1] = temp;
                }
            }
        }
    }

    public void sort_age_desc(int age[]) {
        int n = age.length;
        for (int i = 0; i < n - 1; ++i) {
            for (int j = 0; j < n - i - 1; ++j) {
                if (age[j] < age[j + 1]) {

                    int temp = age[j];
                    age[j] = age[j + 1];
                    age[j + 1] = temp;
                }
            }
        }
    }

    public static void sort_gpa_asc(double gpa[]) {
        int n = gpa.length;
        for (int i = 0; i < n - 1; ++i) {
            for (int j = 0; j < n - i - 1; ++j) {
                if (gpa[j] > gpa[j + 1]) {

                    double temp = gpa[j];
                    gpa[j] = gpa[j + 1];
                    gpa[j + 1] = temp;
                }
            }
        }
    }

    public static void sort_gpa_desc(double gpa[]) {
        int n = gpa.length;
        for (int i = 0; i < n - 1; ++i) {
            for (int j = 0; j < n - i - 1; ++j) {
                if (gpa[j] < gpa[j + 1]) {

                    double temp = gpa[j];
                    gpa[j] = gpa[j + 1];
                    gpa[j + 1] = temp;
                }
            }
        }
    }

    public void sort_name_l_desc(String name[]) {
        int n = name.length;
        for (int i = 0; i < n - 1; ++i) {
            for (int j = 0; j < n - i - 1; ++j) {
                if (name[j].length() < name[j + 1].length()) {

                    String temp = name[j];
                    name[j] = name[j + 1];
                    name[j + 1] = temp;
                }
            }
        }
    }

    private void SortNameLAsc(String name[]) {
        int n = name.length;
        for (int i = 0; i < n - 1; ++i) {
            for (int j = 0; j < n - i - 1; ++j) {
                if (name[j].length() > name[j + 1].length()) {

                    String temp = name[j];
                    name[j] = name[j + 1];
                    name[j + 1] = temp;
                }
            }
        }
    }


}