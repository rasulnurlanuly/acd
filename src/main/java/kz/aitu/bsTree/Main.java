public class Main {

    public static void main(String[] args) {
        BSTree BSTree = new BSTree();

        BSTree.insert(1000, "A");
        BSTree.insert(2000, "B");
        BSTree.insert(500, "C");
        BSTree.insert(1500, "D");
        BSTree.insert(750, "E");
        BSTree.insert(250, "F");
        BSTree.insert(625, "G");
        BSTree.insert(1250, "H");
        BSTree.insert(875, "I");
        BSTree.insert(810, "Z");

        System.out.println("===Print 810 (Z)");
        System.out.println(BSTree.find(810));
        System.out.println("===Print ALLAscending(FCGEZIAHDB)");
//      BSTree.printAllAscending();
        System.out.println("===Print ALL(ACBFEDGIHZ)");
        BSTree.printAll();
        System.out.println("===Removing 1000");
        System.out.println("===Print ALLAscending(FCGEZIHDB)");
//      BSTree.printAllAscending();
        System.out.println("===Print ALL(ICBFEDGZH)");
        BSTree.printAll();

    }
}

