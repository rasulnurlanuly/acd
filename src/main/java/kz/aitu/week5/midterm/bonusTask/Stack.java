public class Stack {
    private Node group;
    private int size = 0;
    public Node getGroup () {
        return group;
    }

    public void push(int data) {
        Node newNode = new Node ();
        newNode.setData(data);
        if (size != 0) {
            newNode.setNext(group);
        }
        group = newNode;
        size++;
    }


    public int pop () {
        Node oldGroup = group;
        group = group.getNext();
        size--;
        return oldGroup.getData();
    }

    public boolean empty () {
        if (size == 0){
            return true;
        }else
            return false;
    }

    public int size () {
        return size;
    }

}