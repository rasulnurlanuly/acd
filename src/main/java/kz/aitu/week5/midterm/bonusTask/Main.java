public class Main {

    public static Stack function(Stack stack_1, Stack stack_2, Stack stack_3) {
        Stack newStack = new Stack();
        int t = 0;
        int g1 = 0;
        int g2 = 0;
        int g3 = 0;
        while((stack_1.getGroup()!=null)||(stack_2.getGroup()!=null)||(stack_3.getGroup()!=null)){
            if(stack_1.getGroup()!=null) {
                g1 = (stack_1.getGroup()).getData();
            } else {
                g1 = 0;
            }
            if(stack_2.getGroup()!=null) {
                g2 = (stack_2.getGroup()).getData();
            } else {
                g2 = 0;
            }
            if(stack_3.getGroup()!=null) {
                g3 = (stack_3.getGroup()).getData();
            } else {
                g3 = 0;
            }
            if((g1)>=(g2)&&(g1)>=(g3)){
                t = stack_1.pop();
                newStack.push(t);
            } else if((g2)>=(g1)&&(g2)>=(g3)){
                t = stack_2.pop();
                newStack.push(t);
            } else if((g3)>=(g1)&&(g3)>=(g1)){
                t = stack_3.pop();
                newStack.push(t);
            }
        }
        Stack newStack2 = new Stack();
        while(newStack.getGroup()!=null) {
            int a = newStack.pop();
            newStack2.push(a);
        }
        return newStack2;

    }

    public static void main(String[] args) {
        Stack stack_1 = new Stack();
        stack_1.push(1);
        stack_1.push(2);
        stack_1.push(17);
        stack_1.push(18);
        stack_1.push(19);
        stack_1.push(20);

        Stack stack_2 = new Stack();
        stack_2.push(3);
        stack_2.push(4);
        stack_2.push(10);
        stack_2.push(15);
        stack_2.push(18);

        Stack stack_3 = new Stack();
        stack_3.push(5);
        stack_3.push(6);
        stack_3.push(20);
        stack_3.push(30);
        stack_3.push(35);
        stack_3.push(40);

        Stack newStack = function(stack_1, stack_2, stack_3);

        Node temp = newStack.getGroup();
        while (temp!=null) {
            System.out.println(temp.getData());
            temp = temp.getNext();
        }

    }
}
