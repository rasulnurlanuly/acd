public class Task706 {
    int a[][] = new int[5][5];
    public void inputData() {
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < 5; i ++) {
            for (int j = 0; j < 5; j ++) {
                a[i][j] = scanner.nextInt();
            }
        }
    }

    public void print() {
        for (int i = 0; i < 5; i ++) {
            for (int j = 0; j < 5; j ++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
    }

    public boolean findPath(int i, int j) {
        if (a[i][j]==0) return false;
        if (i==4 && j==4) {
            a[i][j]=2;
            return true;
        }
        a[i][j]=0;
        boolean flag=false;
        if (i+1!=5) flag=flag||findPath(i+1, j);
        if (i!=0) flag=flag||findPath(i-1, j);
        if (j+1!=5) flag=flag||findPath(i, j+1);
        if (j!=0) flag=flag||findPath(i, j-1);

        if (flag) a[i][j]=2;
        return flag;
    }

    public void run() {
        inputData();
        System.out.println(findPath(0, 0));
        print();
    }
}
