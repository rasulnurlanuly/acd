public class Task703 {
    public static boolean rev(int n) {
        if (n==0) return true;
        Scanner s = new Scanner(System.in);
        String a = s.next();
        if (rev(n-1)) {
            System.out.println(a);
            return true;
        }
        return false;
    }
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int n = s.nextInt();
        rev(n);
    }
}
