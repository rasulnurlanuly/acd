public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int [][] arr = new int [n][n];
        spiral(arr, 0,0,n,n);

        for (int i=0;i<n;i++){
            for (int j=0;j<n;j++){
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static int y = 1;

    public static void spiral(int array[][], int i, int j, int m,int n){
        if (i>=m && j>=n) return;
        for (int p=i; p<n;p++){
            array[i][p] = y;
            y++;
        }
        for (int p = i + 1; p < m; p++)
        {
            array[p][n - 1] =y;
            y++;
        }
        if ((m - 1) != i)
        {
            for (int p = n - 2; p >= j; p--)
            {
                array[m - 1][p] = y;
                y++;
            }
            for (int p = m - 2; p > i; p--)
            {
                array[p][j] = y;
                y++;
            }
        }

        spiral(array, i + 1, j + 1, m - 1, n - 1);
    }
}
