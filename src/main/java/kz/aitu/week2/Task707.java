public class Task7 {
    public static void permutation(String a, String b){
        int n = b.length();
        if (n==0) System.out.println(a);
        else {
            for (int i=0;i<n;i++){
                permutation(a+b.charAt(i), b.substring(0,i)+b.substring(i+1,n));
            }
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String a;
        a = scanner.nextLine();
        permutation("", a);
    }
}
