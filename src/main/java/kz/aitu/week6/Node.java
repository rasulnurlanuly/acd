package week6;

public class Node {
    private String key;
    private String date;
    private Node next;

    public Node(String key, String date) {
        this.key = key;
        this.date = date;
        this.next = null;
    }

    public Node(String key) {
        this.key = key;
    }
    public void setNext(Node next) {
        this.next = next;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return date;
    }

    public void setValue(String date) {
        this.date = date;
    }


    public Node getNext() {
        return next;
    }
}



