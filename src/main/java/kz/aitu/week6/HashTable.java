package week6;

public class HashTable {

    private Node table[];
    private int size;

    public HashTable(int size) {
        table = new Node[size];
        this.size = size;
    }

    public void set(String key, String date) {
        Node node = new Node(key, date);
        int index = key.hashCode() % size;

        if(table[index] == null){
            table[index] = node;
        }
        else{
            Node current = table[index];
            while (current != null) {

                if (current.getKey() == key) {
                    current.setValue(date);
                    return;
                }
                if (current.getNext() == null) {
                    current.setNext(node);
                    return;
                }
                current = current.getNext();
            }
        }
    }
    public String get(String key) {
        int index = key.hashCode() % size;
        Node curr = table[index];
        while(curr != null){
            if (curr.getKey() == key) {
                return table[index].getValue();
            }
            curr = curr.getNext();
        }
        return null ;
    }

    public void remove(String key) {
        table[key.hashCode() % size]=null;
        size--;
    }

    public int getSize() {
        return size;
    }

    public void printAll() {
        for (int i = 0; i < table.length; i++) {
            if (table[i] != null)
                System.out.println(i + "\t " + table[i]);
        }
    }
}


