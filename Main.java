public class Main {

   public static void main(String[] args) {
      LinkedList llist = new LinkedList();

      llist.insertAtFirst(1);
      llist.insertAtFirst(2);
      llist.insertAtFirst(3);
      llist.insertAtFirst(4);
      llist.insertAtFirst(5);

      llist.showList();

      llist.removeAtPosition(2);

      llist.showList();
   }
}
