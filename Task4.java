public class Task4 {

   public static int Buy(int arr[], int n) {
      int toBuy = arr[0];

      for (int i = 1; i < n; i++)
         toBuy = Math.min(toBuy, arr[i]);
      return toBuy;
   }

   public static int Sell(int arr[], int n) {
      int sell = arr[0];

      for (int i = 1; i < n; i++)
         sell = Math.max(sell, arr[i]);
      return sell;
   }

   public static void main(String[] args) {

      int arr[] = { 10, 7, 5, 8, 11 };
      int n = arr.length;
      System.out.println( "Buy - "+ Buy(arr, n));
      System.out.println( "Sell - " + Sell(arr, n));
   }

}
